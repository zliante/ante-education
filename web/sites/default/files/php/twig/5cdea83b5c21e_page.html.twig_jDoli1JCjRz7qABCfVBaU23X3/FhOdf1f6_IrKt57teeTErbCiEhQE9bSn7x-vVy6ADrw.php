<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/geeks/templates/page.html.twig */
class __TwigTemplate_4217248f7bbbbbfc533f417745f6857efcf55590466275923d71f763bb2da09f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 57];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        echo "<header id=\"header\" >
    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
        // line 57
        if ( !twig_test_empty($this->getAttribute(($context["page"] ?? null), "header", []))) {
            // line 58
            echo "                <div class=\"col-12\" role=\"heading\">
                    ";
            // line 59
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 62
        echo "        </div>
    </div>
</header>
<section id=\"header_content\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
        // line 68
        if ( !twig_test_empty($this->getAttribute(($context["page"] ?? null), "header_sidebar", []))) {
            // line 69
            echo "                <div class=\"col-12 col-lg-3\">
                    ";
            // line 70
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_sidebar", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 73
        echo "            ";
        if (twig_test_empty($this->getAttribute(($context["page"] ?? null), "header_sidebar", []))) {
            // line 74
            echo "                <div class=\"col-12 col-lg-12\">
                    ";
            // line 75
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_content", [])), "html", null, true);
            echo "
                </div>
            ";
        } else {
            // line 78
            echo "                <div class=\"col-12 col-sm-9\">
                    ";
            // line 79
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_content", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 82
        echo "        </div>
    </div>
</section>
<section id=\"main\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
        // line 88
        if ( !twig_test_empty($this->getAttribute(($context["page"] ?? null), "left_sidebar", []))) {
            // line 89
            echo "                <div class=\"col-12 col-lg-3\">
                    ";
            // line 90
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "left_sidebar", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 93
        echo "            ";
        if (twig_test_empty($this->getAttribute(($context["page"] ?? null), "left_sidebar", []))) {
            // line 94
            echo "                <div class=\"col-12\">
                    ";
            // line 95
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                </div>
            ";
        } else {
            // line 98
            echo "                <div class=\"col-12 col-lg-9\">
                    ";
            // line 99
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 102
        echo "        </div>
    </div>
</section>
<footer>
    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
        // line 108
        if ( !twig_test_empty($this->getAttribute(($context["page"] ?? null), "footer", []))) {
            // line 109
            echo "                <div class=\"col-12\">
                    ";
            // line 110
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 113
        echo "        </div>
    </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "themes/geeks/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 113,  164 => 110,  161 => 109,  159 => 108,  151 => 102,  145 => 99,  142 => 98,  136 => 95,  133 => 94,  130 => 93,  124 => 90,  121 => 89,  119 => 88,  111 => 82,  105 => 79,  102 => 78,  96 => 75,  93 => 74,  90 => 73,  84 => 70,  81 => 69,  79 => 68,  71 => 62,  65 => 59,  62 => 58,  60 => 57,  55 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
<header id=\"header\" >
    <div class=\"container-fluid\">
        <div class=\"row\">
            {% if page.header is not empty %}
                <div class=\"col-12\" role=\"heading\">
                    {{ page.header }}
                </div>
            {% endif %}
        </div>
    </div>
</header>
<section id=\"header_content\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            {% if page.header_sidebar is not empty %}
                <div class=\"col-12 col-lg-3\">
                    {{ page.header_sidebar }}
                </div>
            {% endif %}
            {% if page.header_sidebar is empty  %}
                <div class=\"col-12 col-lg-12\">
                    {{ page.header_content }}
                </div>
            {% else %}
                <div class=\"col-12 col-sm-9\">
                    {{ page.header_content }}
                </div>
            {% endif %}
        </div>
    </div>
</section>
<section id=\"main\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            {% if page.left_sidebar is not empty %}
                <div class=\"col-12 col-lg-3\">
                    {{ page.left_sidebar }}
                </div>
            {% endif %}
            {% if page.left_sidebar is empty  %}
                <div class=\"col-12\">
                    {{ page.content }}
                </div>
            {% else %}
                <div class=\"col-12 col-lg-9\">
                    {{ page.content }}
                </div>
            {% endif %}
        </div>
    </div>
</section>
<footer>
    <div class=\"container-fluid\">
        <div class=\"row\">
            {% if page.footer is not empty %}
                <div class=\"col-12\">
                    {{ page.footer }}
                </div>
            {% endif %}
        </div>
    </div>
</footer>
", "themes/geeks/templates/page.html.twig", "/app/web/themes/geeks/templates/page.html.twig");
    }
}
