<?php
$databases['default']['default'] = array (
  'database' => '',
  'username' => 'root',
  'password' => '',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

ini_set('max_execution_time', 0);
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
ini_set('memory_limit', '512M');
$settings['twig_debug'] = TRUE;

